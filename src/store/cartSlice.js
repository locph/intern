import { createSlice } from '@reduxjs/toolkit';

const cartSlice = createSlice({
  name: 'cart',
  initialState: {
    cart: [],
  },
  reducers: {
    addToCart: (state, action) => {
      const { _id, color, size } = action.payload;
      const cartIndex = state.cart.findIndex(
        (item) => item._id === _id && item.color === color && item.size === size
      );

      if (cartIndex < 0) {
        state.cart.push(action.payload);
      } else {
        state.cart[cartIndex].quantity += action.payload.quantity;
      }
    },
    changeQuantity: (state, action) => {
      const { _id, color, size, quantity } = action.payload;

      const index = state.cart.findIndex((item) => {
        return item._id === _id && item.color === color && item.size === size;
      });

      if (index >= 0) {
        state.cart[index].quantity = Math.max(1, parseInt(quantity) || 0);
      }
    },
    deleteItem: (state, action) => {
      const { _id, color, size } = action.payload;
      state.cart = state.cart.filter((item) => {
        return item._id !== _id || item.color !== color || item.size !== size;
      });
    },
    clearCart: (state) => {
      state.cart = [];
    },
  },
});

export const {
  addToCart,
  changeQuantity,
  deleteItem,
  clearCart,
} = cartSlice.actions;

export default cartSlice.reducer;
