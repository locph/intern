import React from 'react';
import { Button, Col, Layout, Row, Table, Modal } from 'antd';
import { useSelector, useDispatch } from 'react-redux';

import './Cart.scss';
import QuantityGrpBtn from '../../Components/QuantityGrpBtn/QuantityGrpBtn';
import { changeQuantity, deleteItem } from '../../store/cartSlice';
import { Link } from 'react-router-dom';
import baseAxios from '../../Util/axios';

export default function Cart() {
  const { cart } = useSelector((state) => state.cart);
  const { user } = useSelector((state) => state.user);
  const dispatch = useDispatch();

  const changeQuantityHandler = (product, value) => {
    const newValue = Math.max(1, parseInt(value) || 0);
    const { _id, size, color } = product;

    if (!user) {
      dispatch(changeQuantity({ _id, size, color, quantity: newValue }));
    } else {
      baseAxios.post('/graphql', {
        query: `
          mutation (
            $_id: ID
            $size: String
            $color: String
            $quantity: Int
            ) {
            changeQuantity(
              _id: $_id
              size: $size
              color: $color
              quantity: $quantity
              ) {
              _id
              size
              color
              quantity
            }
          }
        `,
        variables: {
          quantity: newValue,
          _id,
          size,
          color,
        },
      });
    }
  };

  const removeItemFromCart = (product) => {
    Modal.confirm({
      title: 'Are you sure?',
      content: (
        <p>
          Are you sure to delete <strong>{product.name}</strong> from your cart?
        </p>
      ),
      okText: 'DELETE',
      okButtonProps: {
        danger: true,
      },
      cancelText: 'CANCEL',
      closable: true,
      maskClosable: true,
      onOk: () => {
        const { _id, color, size } = product;

        if (!user) {
          dispatch(deleteItem(product));
        } else {
          baseAxios
            .post('/graphql', {
              query: `
              mutation(
                $_id: ID
                $size: String
                $color: String
              ) {
                removefromCart(
                  _id: $_id
                  size: $size
                  color: $color
                ) {
                  _id
                  size
                  color
                }
              }
            `,
              variables: {
                _id,
                color,
                size,
              },
            })
            .catch(console.log);
        }
      },
    });
  };

  const checkoutHandler = () => {
    baseAxios
      .post('/graphql', {
        query: `
        mutation ($products: [InputProduct]) {
          checkout(products: $products)
        }
      `,
        variables: {
          products: cart.map((item) => ({
            _id: item._id,
            color: item.color,
            size: item.size,
            quantity: item.quantity,
          })),
        },
      })
      .then((res) => {
        Modal.success({
          title: 'Success!',
          content: 'Your order has been sent!',
        });
      })
      .catch(console.log);
  };

  const SHIPPING_FEE = 0;

  return (
    <Layout.Content className="container cart-page">
      <h1 style={{ margin: '24px 0', fontWeight: 600 }}>My Bag</h1>
      <Row gutter={16}>
        <Col span={16}>
          <Table
            pagination={false}
            dataSource={cart}
            columns={[
              {
                title: 'Product',
                dataIndex: 'photo',
                key: 'photo',
                render: (text, record, index) => {
                  return (
                    <div className="product-cell">
                      <div className="img-container">
                        <img
                          src={`http://localhost:4808/images/${text}`}
                          alt={text}
                        />
                      </div>
                      <div className="info">
                        <Link to={`/product/${record._id}`}>
                          <h3 className="name">{record.name}</h3>
                        </Link>

                        <div className="btn-grp">
                          <Button disabled type="text">
                            Change
                          </Button>
                          <Button
                            type="text"
                            onClick={() => removeItemFromCart(record)}
                          >
                            Remove
                          </Button>
                        </div>
                      </div>
                    </div>
                  );
                },
              },
              {
                title: 'Color',
                dataIndex: 'color',
                key: 'color',
                render: (text) => (
                  <div
                    style={{
                      width: 30,
                      height: 30,
                      borderRadius: '50%',
                      backgroundColor: text,
                      filter: 'brightness(0.8)',
                      margin: 'auto',
                    }}
                  ></div>
                ),
              },
              {
                title: 'Size',
                dataIndex: 'size',
                key: 'size',
                render: (text) => (
                  <h1 style={{ borderBottom: 0 }}>{text.toUpperCase()}</h1>
                ),
              },
              {
                title: 'Quantity',
                dataIndex: 'quantity',
                key: 'quantity',
                render: (text, record, index) => (
                  <QuantityGrpBtn
                    style={{ margin: 'auto' }}
                    quantity={text}
                    minusBtnClick={() =>
                      changeQuantityHandler(record, text - 1)
                    }
                    plusBtnClick={() => changeQuantityHandler(record, text + 1)}
                    changeQuantity={(e) =>
                      changeQuantityHandler(record, e.target.value)
                    }
                  />
                ),
              },
              {
                title: 'Amount',
                render: (text, record) => (
                  <h3 style={{ fontWeight: 600, borderBottom: 0 }}>
                    ${record.quantity * record.price}
                  </h3>
                ),
              },
            ]}
          />
        </Col>
        <Col span={8}>
          <div className="total">
            <h3 className="title">Total</h3>
            <div className="total-box">
              <div className="total-box-item">
                <span>Shipping and handling:</span>
                <span>{SHIPPING_FEE || 'Free'}</span>
              </div>
              <div className="total-box-item">
                <span>Total product:</span>
                <span>
                  $
                  {cart.reduce((total, { price, quantity }) => {
                    return total + price * quantity;
                  }, 0)}
                </span>
              </div>
              <div className="total-box-item">
                <span>Subtotal:</span>
                <span>
                  $
                  {cart.reduce((total, { price, quantity }) => {
                    return total + price * quantity;
                  }, SHIPPING_FEE)}
                </span>
              </div>
            </div>
            <Button
              block
              danger
              type="primary"
              className="check-out-btn"
              size="large"
              disabled={!user || !cart.length}
              onClick={checkoutHandler}
            >
              Check out
            </Button>
          </div>
        </Col>
      </Row>
    </Layout.Content>
  );
}
