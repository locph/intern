import React, { useEffect, useState } from 'react';
import { Button, Col, Collapse, Layout, Row, Select, Slider } from 'antd';
import InfinityScroll from 'react-infinite-scroll-component';

import './Products.scss';
import { categorires, size, brands, colors } from './Categories';
import CheckboxReverse from '../../Components/Checkbox/CheckboxReverse';
import Card from '../../Components/Card/Card';
import axios from '../../Util/axios';
import { Link } from 'react-router-dom';

const selectSortOption = [
  {
    value: 'name-inc',
    label: 'Name: A - Z',
  },
  {
    value: 'name-dec',
    label: 'Name: Z - A',
  },
  {
    value: 'price-inc',
    label: 'Price: lowest to highest',
  },
  {
    value: 'price-dec',
    label: 'Price: highest to lowest',
  },
];

export default function Products() {
  const [products, setProducts] = useState([]);
  const [totalProducts, setTotalProducts] = useState(1);
  const [loading, setLoading] = useState(false);
  const [activeFilter, setActiveFilter] = useState({
    size: '',
    brand: '',
    color: '',
    category: '',
    sortby: 'name-inc',
    available: 0,
    page: 1,
    price: [0, 500],
  });

  useEffect(() => {
    setLoading(true);
    const queryArray = [];
    for (const key in activeFilter) {
      queryArray.push(`${key}=${activeFilter[key]}`);
    }

    axios
      .get(`/shop/products?${queryArray.join('&')}`)
      .then((res) => {
        setTotalProducts(res.data.totalProducts);
        setProducts((oldProducts) => [...oldProducts, ...res.data.products]);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [activeFilter]);

  const toggleActiveHandler = (key, value) => {
    setProducts([]);
    setActiveFilter((pre) => ({ ...pre, [key]: value, page: 1 }));
  };

  const nextPageHandler = () => {
    setActiveFilter((oldProducts) => ({
      ...oldProducts,
      page: oldProducts.page + 1,
    }));
  };

  return (
    <Layout.Content className="container products-page">
      <Row gutter={[16, 16]}>
        <Col className="sidebar" span={4}>
          <h2>Category</h2>
          <ul className="category">
            {[{ value: '', label: 'All dresses' }, ...categorires].map(
              (category) => (
                <li key={category.label}>
                  <Button
                    type="link"
                    className={
                      'category-btn ' +
                      (category.value === activeFilter.category ? 'active' : '')
                    }
                    onClick={() =>
                      toggleActiveHandler('category', category.value)
                    }
                  >
                    {category.label}
                  </Button>
                </li>
              )
            )}
          </ul>

          <h2>Filter</h2>
          <Collapse expandIconPosition="right" className="collapse">
            <Collapse.Panel header="Size" key="size" className="size">
              {size.map((s) => (
                <button
                  key={s.label}
                  className={activeFilter.size === s.value ? 'active' : ''}
                  onClick={() => toggleActiveHandler('size', s.value)}
                >
                  {s.label}
                </button>
              ))}
            </Collapse.Panel>
            <Collapse.Panel header="Color" key="color" className="color">
              {colors.map((color) => (
                <span key={color.label}>
                  <button
                    onClick={() => toggleActiveHandler('color', color.value)}
                    style={{
                      backgroundColor: color.value,
                      borderColor:
                        color.value === activeFilter.color
                          ? 'var(--pumpkin-orange)'
                          : color.value,
                    }}
                  />
                </span>
              ))}
            </Collapse.Panel>
            <Collapse.Panel header="Brand" key="brand">
              {brands.map((brand) => (
                <CheckboxReverse
                  label={brand.label}
                  key={brand.label}
                  checked={activeFilter.brand === brand.value}
                  onClick={() => toggleActiveHandler('brand', brand.value)}
                />
              ))}
            </Collapse.Panel>
            <Collapse.Panel header="Price" key="price" className="price">
              <Slider
                range
                step={10}
                defaultValue={activeFilter.price}
                min={0}
                max={500}
                marks={{ 0: '$0', 500: '$500' }}
                onChange={(range) => toggleActiveHandler('price', range)}
              />
            </Collapse.Panel>
            <Collapse.Panel header="Available" key="available">
              <CheckboxReverse
                label="In stock"
                checked={activeFilter.available === 1}
                onClick={() => toggleActiveHandler('available', 1)}
              />
              <CheckboxReverse
                label="Out of stock"
                checked={activeFilter.available === -1}
                onClick={() => toggleActiveHandler('available', -1)}
              />
            </Collapse.Panel>
          </Collapse>
        </Col>

        <Col span={20}>
          <div style={{ marginBottom: 24 }}>
            <Select
              optionLabelProp="title"
              value={activeFilter.sortby}
              onChange={(value) => toggleActiveHandler('sortby', value)}
              size="large"
              style={{
                minWidth: 300,
                fontWeight: 500,
              }}
            >
              {selectSortOption.map((option) => (
                <Select.Option
                  key={option.value}
                  title={
                    <span>
                      Sort by: <strong>{option.label}</strong>
                    </span>
                  }
                  value={option.value}
                >
                  {option.label}
                </Select.Option>
              ))}
            </Select>
          </div>

          <InfinityScroll
            dataLength={products.length}
            next={nextPageHandler}
            hasMore={totalProducts > products.length}
            loader={<h4>Loading...</h4>}
            style={{ overflow: 'visible' }}
          >
            <Row gutter={[16, 16]}>
              {products.map((product) => (
                <Col key={product.name} span={6} style={{ paddingBottom: 30 }}>
                  <Link to={`/product/${product._id}`}>
                    <Card
                      key={product._id}
                      img={product.photos[0]}
                      {...product}
                    />
                  </Link>
                </Col>
              ))}
            </Row>
          </InfinityScroll>
          {!products.length && !loading && (
            <h1
              style={{
                textAlign: 'center',
                marginTop: 24,
                color: 'var(--greyish)',
              }}
            >
              No result found!
            </h1>
          )}
        </Col>
      </Row>
    </Layout.Content>
  );
}
