export const categorires = [
  {
    label: 'Rompers / Jumpsuits',
    value: 'rompers-jumpsuits',
  },
  {
    label: 'Casual dresses',
    value: 'casual-dresses',
  },
  {
    label: 'Going out dresses',
    value: 'going-out-dresses',
  },
  {
    label: 'Party / Ocassion dresses',
    value: 'party-ocassion-dresses',
  },
  {
    label: 'Mini dresses',
    value: 'mini-dresses',
  },
  {
    label: 'Maxi / Midi dresses',
    value: 'maxi-midi-dresses',
  },
  {
    label: 'Sets',
    value: 'sets',
  },
];

export const brands = [
  {
    label: 'Zara',
    value: 'zara',
  },
  {
    label: 'H&M',
    value: 'h-m',
  },
  {
    label: 'Pull&Bear',
    value: 'pull-bear',
  },
  {
    label: 'Dior',
    value: 'dior',
  },
  {
    label: 'Chanel',
    value: 'chanel',
  },
];

export const size = [
  { label: 'S', value: 's' },
  { label: 'M', value: 'm' },
  { label: 'L', value: 'l' },
  { label: 'XL', value: 'xl' },
  { label: 'XXL', value: 'xxl' },
];

export const colors = [
  { label: 'Green', value: 'green' },
  { label: 'Blue', value: 'blue' },
  { label: 'White', value: 'white' },
  { label: 'Black', value: 'black' },
  { label: 'Red', value: 'red' },
];
