import React from 'react';
import { Col, Layout, Row } from 'antd';

import './Main.scss';
import bannerImg from '../../assets/images/des2x-100_24.jpg';
import img1 from '../../assets/images/1.jpg';
import img2 from '../../assets/images/3.jpg';
import img3 from '../../assets/images/6.jpg';
import img4 from '../../assets/images/18.jpg';
import { Link } from 'react-router-dom';

const shopCategories = [
  {
    img: img1,
    title: 'Men',
  },
  {
    img: img2,
    title: 'Ladies',
  },
  {
    img: img3,
    title: 'Girls',
  },
  {
    img: img4,
    title: 'Kids',
  },
];

export default function Main() {
  return (
    <Layout.Content className="container mainpage">
      <Row gutter={[16, 16]}>
        <Col span={24}>
          <img src={bannerImg} alt="banner" />
        </Col>
        {shopCategories.map((item) => (
          <Col span={6} key={item.title} className="card">
            <div className="card-container">
              <img src={item.img} alt={item.title} />
              <div className="description">
                <h1 className="title">{item.title}</h1>
                <Link to="/products">
                  <button
                    type="primary"
                    size="large"
                    className="primary-button"
                  >
                    SHOP NOW
                  </button>
                </Link>
              </div>
            </div>
          </Col>
        ))}
      </Row>
    </Layout.Content>
  );
}
