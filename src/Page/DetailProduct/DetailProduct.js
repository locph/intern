import React, { useEffect, useState } from 'react';
import {
  Col,
  Layout,
  Row,
  Rate,
  Button,
  Divider,
  Modal,
  Tooltip,
  notification,
} from 'antd';
import Slider from 'react-slick';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import './DetailProduct.scss';
import axios from '../../Util/axios';
import { size, colors, brands } from '../Products/Categories';
import { addToCart } from '../../store/cartSlice';
import QuantityGrpBtn from '../../Components/QuantityGrpBtn/QuantityGrpBtn';
import PostComment from '../../Components/PostComment/PostComment';
import Comment from '../../Components/PostComment/Comment';

function DetailProduct(props) {
  const [nav1, setNav1] = useState(null);
  const [nav2, setNav2] = useState(null);
  const [product, setProduct] = useState(null);
  const [moreFromProducts, setMoreFromProducts] = useState([]);
  const [alsoLikeProducts, setAlsoLikeProducts] = useState([]);
  const [ratings, setRatings] = useState([]);
  const [order, setOrder] = useState({
    size: '',
    color: '',
    quantity: 1,
  });

  const dispatch = useDispatch();
  const { user } = useSelector((state) => state.user);

  useEffect(() => {
    window.scrollTo(0, 0);
    const { productId } = props.match.params;
    setOrder({
      size: '',
      color: '',
      quantity: 1,
    });

    axios
      .get(`/shop/product/${productId}`)
      .then((res) => {
        setProduct(res.data.product);
        setMoreFromProducts(res.data.moreFromProduct);
        setAlsoLikeProducts(res.data.alsoLikeProduct);
      })
      .catch((err) => {
        console.log(err);
      });

    axios
      .get(`/rating/${productId}`)
      .then((res) => {
        setRatings(res.data.rating);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [props]);

  const changeDetailProductOrderHandler = (key, value) => {
    setOrder((oldOrder) => ({ ...oldOrder, [key]: value }));
  };

  const openErrorModalHandler = (errorText) => {
    Modal.error({
      title: "Can't add to cart!",
      content: `You need to choose ${errorText}!`,
    });
  };

  const openSuccessNotificationHandler = () => {
    notification.success({
      message: 'Add to cart success!',
      description: 'This product has been added to your cart.',
      duration: 5,
      top: 60,
    });
  };

  const addToCartHandler = () => {
    const checkList = ['color', 'size'];
    for (const key of checkList) {
      if (!order[key]) {
        return openErrorModalHandler(key);
      }
    }

    const { _id, name, price, photos } = product;

    if (user) {
      axios
        .post('/graphql', {
          query: `mutation (
              $_id: ID! 
              $color: String! 
              $quantity: Int 
              $size: String 
            ) {
						addToCart (
							_id: $_id
							color: $color
							quantity: $quantity
							size: $size
						) {
							_id
							name
							price
							quantity
							color
							size
							photo
							key
              userId
						}
					}`,
          variables: { _id, ...order },
        })
        .then(openSuccessNotificationHandler)
        .catch((err) => {
          console.log(err);
        });
    } else {
      const addToCartProduct = {
        _id,
        name,
        price,
        ...order,
        photo: photos[0],
        key: _id + order.color + order.size,
      };
      openSuccessNotificationHandler();
      dispatch(addToCart(addToCartProduct));
    }
  };

  const postNewRatingHandler = (rating) => {
    setRatings((pre) => {
      const newRating = { ...rating, userId: { ...user } };
      return [newRating, ...pre];
    });
  };

  const deletePostHandler = (ratingId) => {
    setRatings((pre) => pre.filter((r) => r._id !== ratingId));
  };

  if (!product) {
    return <h1>Loading</h1>;
  }

  return (
    <Layout.Content className="container detail-product-page">
      <h1 style={{ marginTop: 16 }}>Detail products</h1>
      <Row gutter={[16, 16]}>
        <Col span={3}>
          <Slider
            dots={false}
            infinite={true}
            slidesToShow={3}
            slidesToScroll={1}
            vertical={true}
            verticalSwiping={true}
            arrows={false}
            asNavFor={nav2}
            ref={setNav1}
          >
            {product.photos.map((photo) => (
              <div key={photo} className="img-list">
                <img
                  alt={photo}
                  src={`http://localhost:4808/images/${photo}`}
                  style={{ width: '100%' }}
                />
              </div>
            ))}
          </Slider>
        </Col>
        <Col span={9}>
          <Slider
            dots={false}
            infinite={true}
            autoplay={true}
            autoplaySpeed={3000}
            fade={true}
            speed={500}
            slidesToShow={1}
            slidesToScroll={1}
            arrows={false}
            asNavFor={nav1}
            ref={setNav2}
          >
            {product.photos.map((photo) => (
              <div key={photo} className="img-list">
                <img
                  alt={photo}
                  src={`http://localhost:4808/images/${photo}`}
                  style={{ width: '100%' }}
                />
              </div>
            ))}
          </Slider>
        </Col>
        <Col span={9} className="info">
          <div style={{ padding: '0 18px' }}>
            <h1 className="title" style={{ margin: 0 }}>
              {product.name}
            </h1>

            <h1 className="price" level={2} style={{ margin: 0 }}>
              ${product.price}
            </h1>

            <div className="content">
              <Rate
                value={Math.round(
                  product.rating.totalScore / product.rating.totalRating
                )}
                allowHalf
                disabled
                style={{ fontSize: 16, marginRight: 16 }}
              />
              <span className="review">
                {product.rating.totalRating} Reviews
              </span>
            </div>

            <div className="content size">
              <h1>Size</h1>
              {size.map((s) => (
                <button
                  key={s.value}
                  className={s.value === order.size ? 'active' : ''}
                  onClick={() =>
                    changeDetailProductOrderHandler('size', s.value)
                  }
                  disabled={!product.size.includes(s.value)}
                >
                  {s.label}
                </button>
              ))}
            </div>

            <div className="content color">
              <h1>Color</h1>
              {colors
                .filter((color) => product.color.includes(color.value))
                .map((color) => (
                  <span key={color.label}>
                    <button
                      onClick={() =>
                        changeDetailProductOrderHandler('color', color.value)
                      }
                      style={{
                        backgroundColor: color.value,
                        borderColor:
                          color.value === order.color
                            ? 'var(--pumpkin-orange)'
                            : color.value,
                      }}
                    />
                  </span>
                ))}
            </div>

            <div className="content quantity">
              <h1>Quantity</h1>
              <QuantityGrpBtn
                quantity={order.quantity}
                minusBtnClick={() => {
                  changeDetailProductOrderHandler(
                    'quantity',
                    Math.max(1, order.quantity - 1)
                  );
                }}
                plusBtnClick={() => {
                  changeDetailProductOrderHandler(
                    'quantity',
                    order.quantity + 1
                  );
                }}
                changeQuantity={(e) =>
                  setOrder((pre) => {
                    return { ...pre, quantity: e.target.value };
                  })
                }
              />
            </div>

            <Button
              type="primary"
              block
              size="large"
              className="cart-btn"
              onClick={addToCartHandler}
              disabled={
                !product.quantity || Object.values(order).some((item) => !item)
              }
            >
              Add to cart
            </Button>

            <Divider style={{ borderWidth: 3 }} />

            <p style={{ fontWeight: 500 }}>{product.description}</p>
          </div>
        </Col>
        <Col span={3}>
          <h1>
            More from{' '}
            {brands.find((brand) => brand.value === product.brand).label}
          </h1>
          <Slider
            dots={false}
            infinite={true}
            slidesToShow={3}
            slidesToScroll={1}
            vertical={true}
            verticalSwiping={true}
            arrows={false}
            autoplay={true}
            autoplaySpeed={3000}
          >
            {moreFromProducts.map((product) => (
              <div key={product._id}>
                <Link to={`/product/${product._id}`}>
                  <Tooltip title={product.name} placement="bottom">
                    <img
                      alt={product._id}
                      src={`http://localhost:4808/images/${product.photos[0]}`}
                      style={{ width: '90%' }}
                    />
                  </Tooltip>
                </Link>
              </div>
            ))}
          </Slider>
        </Col>
      </Row>

      <Divider orientation="left">Reviews</Divider>
      <Row className="review" gutter={16}>
        {user && (
          <PostComment
            productId={props.match.params.productId}
            postRating={postNewRatingHandler}
          />
        )}
        {ratings.map((rating) => (
          <Comment
            key={rating._id}
            {...rating}
            isOwner={user && rating.userId._id === user._id}
            productId={props.match.params.productId}
            deletePostHandler={deletePostHandler}
          />
        ))}
      </Row>
      {!ratings.length && (
        <h1
          style={{
            textAlign: 'center',
            margin: '24px auto 40px',
            color: 'var(--greyish)',
          }}
        >
          No reviews!
        </h1>
      )}
      <Divider orientation="left">Also you may like</Divider>
      <Row gutter={16}>
        {alsoLikeProducts.map((product) => (
          <Col span={4} key={product._id}>
            <Link to={`/product/${product._id}`}>
              <img
                src={`http://localhost:4808/images/${product.photos[0]}`}
                alt={product.name}
                style={{ width: '100%' }}
              />
              <h4 className="also-title">{product.name}</h4>
            </Link>
          </Col>
        ))}
      </Row>
    </Layout.Content>
  );
}

export default DetailProduct;
