import { Layout } from 'antd';
import { Switch, Route, Redirect } from 'react-router-dom';

import './App.scss';
import Header from './Components/Header/Header';
import Cart from './Page/Cart/Cart';
import DetailProduct from './Page/DetailProduct/DetailProduct';
import Main from './Page/Main/Main';
import Products from './Page/Products/Products';

function App(props) {

  return (
    <Layout className="app">
      <Header />

      <Switch>
        <Route path="/products" component={Products} />
        <Route path="/product/:productId" exact component={DetailProduct} />
        <Route path="/cart" exact component={Cart} />
        <Route path="/" exact component={Main} />
        <Redirect to="/" />
      </Switch>
    </Layout>
  );
}

export default App;
