import React from 'react';
import { MinusOutlined, PlusOutlined } from '@ant-design/icons';

import './QuantityGrpBtn.scss';

export default function QuantityGrpBtn(props) {
	return (
		<div className="quantity-btn-group" style={props.style || {}}>
			<button
				className="quantity-btn quantity-btn-item"
				onClick={props.minusBtnClick}
			>
				<MinusOutlined />
			</button>
			<span className="quantity-btn">
				<input
					// type="number"
					// min={1}
					// max={999}
					value={props.quantity}
					onChange={props.changeQuantity}
				/>
			</span>
			<button
				className="quantity-btn quantity-btn-item"
				onClick={props.plusBtnClick}
			>
				<PlusOutlined />
			</button>
		</div>
	);
}
