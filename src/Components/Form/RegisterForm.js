import React, { useState } from 'react';
import { Button, Form, Input, Typography } from 'antd';

import axios from '../../Util/axios';

export default function RegisterForm() {
  const [disableBtn, setDisableBtn] = useState(true);
  const [error, setError] = useState('');
  const [success, setSuccess] = useState('');
  const [loading, setLoading] = useState(false);

  const changeInputHandler = (field, form) => {
    setDisableBtn(!form.every((item) => item.touched && !item.errors.length));
  };

  const submitRegisterFormHandler = (form) => {
    setSuccess('');
    setError('');
    setLoading(true);
    axios
      .post('/auth/sign-up', form)
      .then((res) => {
        setSuccess(res.data.message);
        setLoading(false);
      })
      .catch((err) => {
        const textError = err.response
          ? err.response.data.message
          : 'Something went wrong. Please try again later.';
        setError(textError);
        setLoading(false);
      });
  };

  return (
    <Form
      name="Register"
      layout="vertical"
      onFieldsChange={changeInputHandler}
      requiredMark="optional"
      onFinish={submitRegisterFormHandler}
    >
      <Typography.Title level={2}>Register</Typography.Title>

      <div className="feedback">
        {!!error && <Typography.Text type="danger">{error}</Typography.Text>}
        {!!success && (
          <Typography.Text type="success">{success}</Typography.Text>
        )}
      </div>

      <Form.Item
        label="NAME"
        name="name"
        rules={[
          {
            required: true,
            message: 'Please enter a valid name!',
          },
        ]}
      >
        <Input size="large" placeholder="Enter your name..." />
      </Form.Item>

      <Form.Item
        label="E-MAIL"
        name="email"
        rules={[
          {
            required: true,
            type: 'email',
            message: 'The input is not valid E-mail!',
          },
        ]}
      >
        <Input size="large" placeholder="Enter your email..." />
      </Form.Item>

      <Form.Item
        label="PASSWORD"
        name="password"
        rules={[
          {
            required: true,
            message: 'Your password must be more than 6 characters!',
            min: 6,
          },
        ]}
      >
        <Input
          type="password"
          size="large"
          placeholder="Enter your password..."
        />
      </Form.Item>

      <div className="discuss">
        <p>
          By creating an account you agree to the <br />
          <span className="orange-btn">Term of Service</span> and{' '}
          <span className="orange-btn">Privary Policy</span>
        </p>
      </div>

      <Form.Item>
        <Button
          loading={loading}
          type="primary"
          htmlType="submit"
          disabled={disableBtn}
          className="submit-button"
        >
          Register
        </Button>
      </Form.Item>
    </Form>
  );
}
