import React, { useState } from 'react';
import { Button, Form, Input, Typography } from 'antd';
import { useDispatch } from 'react-redux';

import { login } from '../../store/userSlice';
import axios, { setAuthHeader } from '../../Util/axios';
import Checkbox from '../Checkbox/Checkbox';

export default function LoginForm() {
  const [disableBtn, setDisableBtn] = useState(true);
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const [remember, setRemember] = useState(false);

  const dispatch = useDispatch();

  const changeInputHandler = (field, form) => {
    setDisableBtn(!form.every((item) => item.touched && !item.errors.length));
  };

  const submitRegisterFormHandler = (form) => {
    setError('');
    setLoading(true);
    axios
      .post('/auth/log-in', form)
      .then((res) => {
        if (remember) {
          localStorage.setItem('refreshToken', res.data.refreshToken);
        }
        sessionStorage.setItem('refreshToken', res.data.refreshToken);
        setLoading(false);
        setAuthHeader(res.data.token);
        dispatch(login(res.data));
      })
      .catch((err) => {
        const textError = err.response
          ? err.response.data.message
          : 'Something went wrong. Please try again later.';
        setError(textError);
        setLoading(false);
      });
  };

  return (
    <Form
      name="Login"
      layout="vertical"
      onFieldsChange={changeInputHandler}
      requiredMark="optional"
      onFinish={submitRegisterFormHandler}
      validateTrigger={['onBlur']}
    >
      <Typography.Title level={2}>Log In</Typography.Title>

      <div className="feedback">
        {!!error && <Typography.Text type="danger">{error}</Typography.Text>}
      </div>

      <Form.Item
        label="E-MAIL"
        name="email"
        rules={[
          {
            required: true,
            type: 'email',
            message: 'The input is not valid E-mail!',
          },
        ]}
      >
        <Input size="large" placeholder="Enter your email..." />
      </Form.Item>

      <Form.Item
        label="PASSWORD"
        name="password"
        rules={[
          {
            required: true,
            message: 'Your password must be more than 6 characters!',
            min: 6,
          },
        ]}
      >
        <Input
          type="password"
          size="large"
          placeholder="Enter your password..."
        />
      </Form.Item>

      <div className="forgot">
        <Checkbox
          label="Keep me login"
          checked={remember}
          onClick={() => setRemember((pre) => !pre)}
        />
        <span className="forgot-btn">Forgot your password?</span>
      </div>

      <Form.Item>
        <Button
          loading={loading}
          type="primary"
          htmlType="submit"
          disabled={disableBtn}
          className="submit-button"
        >
          Log in
        </Button>
      </Form.Item>
    </Form>
  );
}
