import React, { useEffect } from 'react';
import { Avatar, Dropdown, Menu, Modal } from 'antd';
import { gql } from '@apollo/client';
import { graphql } from '@apollo/client/react/hoc';
import { useDispatch } from 'react-redux';

import {
  addToCart,
  changeQuantity,
  deleteItem,
  clearCart,
} from '../../store/cartSlice';
import baseAxios from '../../Util/axios';

function UserLogined(props) {
  const dispatch = useDispatch();

  useEffect(() => {
    const unsubscribe = props.data.subscribeToMore({
      document: gql`
        subscription($userId: String) {
          listenAddToCart(userId: $userId) {
            product {
              _id
              name
              price
              quantity
              color
              size
              photo
              key
            }
            type
          }
        }
      `,
      variables: {
        userId: props.user._id,
      },
      updateQuery: (prev, { subscriptionData }) => {
        const { product, type } = subscriptionData.data.listenAddToCart;
        // console.log(product, type);
        switch (type) {
          case 'ADD_PRODUCT':
            dispatch(addToCart(product));
            break;

          case 'REMOVE_PRODUCT':
            dispatch(deleteItem(product));
            break;

          case 'CHANGE_QUANTITY_PRODUCT':
            dispatch(changeQuantity(product));
            break;

          case 'CLEAR_CART':
            dispatch(clearCart());
            break;

          case 'FORCE_LOGOUT':
            props.logoutHandler();
            break;

          default:
            break;
        }
        return prev;
      },
    });

    return unsubscribe;
  }, [props, dispatch]);

  const openModalHandler = () => {
    Modal.confirm({
      title: 'Log out',
      content: 'Do you want to logout on all devices?',
      okButtonProps: {
        danger: true,
      },
      cancelButtonProps: {
        danger: true,
      },
      okText: 'Log out all devices',
      cancelText: 'Just this device',
      onOk: () => {
        baseAxios.post('/graphql', {
          query: `
            mutation {
              logoutAllDevices
            }
          `,
        });
      },
      onCancel: () => {
        baseAxios.post('/graphql', {
          query: `
            mutation Logout($refreshToken: String) {
              logoutDevice(refreshToken: $refreshToken)
            }
          `,
          variables: {
            refreshToken: sessionStorage.getItem('refreshToken'),
          },
        });
        props.logoutHandler();
      },
    });
  };

  return (
    <Dropdown
      trigger={['click']}
      placement="bottomRight"
      overlay={
        <Menu style={{ width: 180 }}>
          {/* <Menu.Item style={{ padding: 18 }}>Account setting</Menu.Item> */}
          <Menu.Item style={{ padding: 18 }} onClick={openModalHandler} danger>
            Logout
          </Menu.Item>
        </Menu>
      }
    >
      <Avatar style={{ cursor: 'pointer', marginRight: 16 }}>
        {props.user.name[0].toUpperCase()}
      </Avatar>
    </Dropdown>
  );
}

const CART_QUERY = gql`
  {
    hello
  }
`;

export default graphql(CART_QUERY)(UserLogined);
