import React from 'react';

import checkbox from '../../assets/images/check-box.svg';
import './Checkbox.scss';

export default function CheckboxReverse(props) {
  return (
    <div
      className={'checkbox-reverse' + (props.checked ? ' active' : '')}
      onClick={props.onClick}
    >
      <span className="label">{props.label}</span>
      <img src={checkbox} alt="checkbox" />
      <div className="holder"></div>
    </div>
  );
}
