import React from 'react';

import './Checkbox.scss';

export default function Checkbox(props) {
  return (
    <label className="checkbox">
      {props.label}
      <input type="checkbox" checked={props.checked} onChange={props.onClick} />
      <span className="checkmark"></span>
    </label>
  );
}
