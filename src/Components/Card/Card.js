import React from 'react';

import './Card.scss';

export default function Card(props) {
  const clickQuickShopHandler = (e) => {
    // e.preventDefault();
    console.log('clicked');
  };

  return (
    <div className="card">
      <div className="img-container">
        <img
          src={`http://localhost:4808/images/${props.img}`}
          alt={props.name}
        />
        {!props.quantity ? (
          <span className="sold-out">Sold out</span>
        ) : (
          <div className="quick-shop">
            <button onClick={clickQuickShopHandler}>+ Quick shop</button>
          </div>
        )}
      </div>
      <h3 className="card-title">{props.name}</h3>
      <p className="price">${props.price}</p>
    </div>
  );
}
