import React, { Fragment, useEffect, useState } from 'react';
import { Button, Modal } from 'antd';
import { useDispatch } from 'react-redux';

import { login } from '../../store/userSlice';
import axios, { setAuthHeader } from '../../Util/axios';
import RegisterForm from '../Form/RegisterForm';
import LoginForm from '../Form/LoginForm';

export default function Unauthenticate() {
  const [visibleModal, setVisibleModal] = useState(false);
  const [isLogin, setIsLogin] = useState(false);
  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    const refreshToken = localStorage.getItem('refreshToken');

    if (refreshToken) {
      setLoading(true);
      axios
        .post('/auth/auto-log-in', { refreshToken })
        .then((res) => {
          setLoading(false);
          setAuthHeader(res.data.token);
          sessionStorage.setItem('refreshToken', refreshToken);
          dispatch(login({ ...res.data, refreshToken }));
        })
        .catch((err) => {
          setLoading(false);
          localStorage.removeItem('refreshToken');
        });
    }
  }, [dispatch]);

  const openModalHandler = (isLogin) => {
    setIsLogin(isLogin);
    setVisibleModal(true);
  };

  const closeModalHandler = () => {
    setVisibleModal(false);
  };

  const toggleLoginModeHandler = () => {
    setIsLogin((prev) => !prev);
  };

  return (
    <Fragment>
      <Button
        loading={loading}
        className="register-btn"
        onClick={() => openModalHandler(false)}
      >
        Register
      </Button>
      <Button
        loading={loading}
        className="login-btn"
        onClick={() => openModalHandler(true)}
      >
        Log In
      </Button>

      <Modal
        footer={
          <p>
            {!isLogin ? 'Do you have an account? ' : "Don't have an account? "}
            <span className="orange-btn" onClick={toggleLoginModeHandler}>
              {!isLogin ? 'Log in' : 'Register'}
            </span>
          </p>
        }
        centered
        visible={visibleModal}
        onCancel={closeModalHandler}
        className="modal-auth"
        width={560}
      >
        {isLogin ? <LoginForm /> : <RegisterForm />}
      </Modal>
    </Fragment>
  );
}
