import React, { useEffect, useState } from 'react';
import { Input, Dropdown, Empty } from 'antd';
import { SearchOutlined } from '@ant-design/icons';

import axios from '../../Util/axios';
import { Link } from 'react-router-dom';

let timer;

export default function Search() {
  const [products, setProducts] = useState([]);
  const [searchValue, setSearchValue] = useState('');

  useEffect(() => {
    if (searchValue) {
      clearTimeout(timer);

      timer = setTimeout(() => {
        axios
          .get(`/shop/products/search?name=${searchValue}`)
          .then((res) => {
            setProducts(res.data.products);
          })
          .catch((err) => {
            console.log(err);
          });
      }, 300);
    }
  }, [searchValue]);

  const overlay = (
    <ul style={{ padding: 12 }}>
      {products.map((product) => {
        return (
          <li key={product._id} style={{ justifyContent: 'normal' }}>
            <img
              src={`http://localhost:4808/images/${product.photos[0]}`}
              alt={product.name}
              style={{ width: 40 }}
            />
            <Link
              to={`/product/${product._id}`}
              style={{
                margin: 'auto 12px',
                fontWeight: 600,
                color: 'var(--greyish-brown)',
              }}
            >
              {product.name}
            </Link>
          </li>
        );
      })}
      {!products.length && (
        <li style={{ display: 'flex' }}>
          <Empty style={{margin: 'auto'}} />
        </li>
      )}
    </ul>
  );

  return (
    <Dropdown
      trigger={['click']}
      overlay={overlay}
      overlayClassName="cart-menu"
      placement="bottomCenter"
    >
      <Input
        placeholder="Search"
        size="large"
        suffix={<SearchOutlined style={{ fontSize: 24 }} />}
        value={searchValue}
        onChange={(e) => setSearchValue(e.target.value)}
      />
    </Dropdown>
  );
}
