import React from 'react';
import { Button } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

import './Header.scss';
import logo from '../../assets/images/logo.svg';
import arrowDownLogo from '../../assets/images/arrow.svg';
import Unauthenticate from './Unauthenticate';
import CartBtn from './Cart';
import { clearAuthHeader } from '../../Util/axios';
import { logout } from '../../store/userSlice';
import Search from './Search';
import UserLogined from '../UserLogined/UserLogined';

export default function Header() {
  const { user } = useSelector((state) => state.user);
  const dispatch = useDispatch();

  const logoutHandler = () => {
    localStorage.removeItem('refreshToken');
    clearAuthHeader();
    dispatch(logout());
  };

  return (
    <div className="main-header">
      <div className="top container">
        <Search />

        <Link to="/">
          <img src={logo} alt="logo" />
        </Link>

        <div className="btn-group">
          {!user ? (
            <Unauthenticate />
          ) : (
            <UserLogined logoutHandler={logoutHandler} user={user} />
          )}

          <CartBtn />
        </div>
      </div>

      <div className="navbar">
        <nav>
          {['Men', 'Ladies', 'Girls', 'Boys'].map((category) => (
            <Button key={category} type="link" size="large">
              {category} <img src={arrowDownLogo} alt="arrow" />
            </Button>
          ))}
        </nav>
        <div className="list">
          <ul>
            {[
              'Tops',
              'Bottoms',
              'Dresses',
              'Jackets',
              'Shoes',
              'Accessories',
              'Sale',
            ].map((category) => (
              <li key={category}>
                <Link to="/products">{category}</Link>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
}
