import React from 'react';
import { Badge, Button, Dropdown, Result } from 'antd';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import cartIcon from '../../assets/images/cart.svg';

export default function Cart() {
  const { cart } = useSelector((state) => state.cart);

  const overlay = (
    <ul>
      {cart.map((item) => (
        <li key={item._id + item.color + item.size}>
          <div className="img-container">
            <img
              src={`http://localhost:4808/images/${item.photo}`}
              alt={item.name}
            />
          </div>
          <div className="info">
            <h4 style={{ fontWeight: 600 }}>{item.name}</h4>
            <div className="detail-info">
              <span>${item.price * item.quantity}</span>
              <span>
                {item.size.toUpperCase()} - {item.color} - {item.quantity} pcs
              </span>
            </div>
          </div>
        </li>
      ))}
      {!cart.length && (
        <li
          style={{
            textAlign: 'center',
            display: 'flex',
            justifyContent: 'center',
          }}
        >
          <Result
            status="404"
            title="Nothing in your cart"
            style={{
              padding: 0,
            }}
          />
        </li>
      )}
      <li className="last-item">
        <Link to="/cart">
          <Button type="link" size="large">
            View cart
          </Button>
        </Link>
      </li>
    </ul>
  );

  return (
    <Dropdown
      trigger={['click']}
      overlay={overlay}
      overlayClassName="cart-menu"
    >
      <button className="cart-btn">
        <Badge count={cart.length}>
          <img src={cartIcon} alt="cart" />
        </Badge>
      </button>
    </Dropdown>
  );
}
