import React, { Fragment, useState } from 'react';
import { Col, Input, Rate } from 'antd';

import './PostComment.scss';
import axios from '../../Util/axios';

export default function PostComment(props) {
  const [comment, setComment] = useState({ title: '', comment: '' });
  const [rating, setRating] = useState(0);

  const changeInputHandler = (e) => {
    setComment((pre) => ({ ...pre, [e.target.name]: e.target.value }));
  };

  const postCommentHandler = () => {
    axios
      .post(`/rating/${props.productId}`, { ...comment, rating })
      .then((res) => {
        props.postRating(res.data);
        setComment({ title: '', comment: '' });
        setRating(0);
      })
      .catch((err) => {
        // console.log(err);
      });
  };

  return (
    <Fragment>
      <Col offset={1} span={4}>
        <div className="name-container">
          <h3 style={{ fontWeight: 600 }}>You</h3>
        </div>
      </Col>
      <Col span={16}>
        <div className="comment-container">
          <Input
            placeholder="TITLE"
            size="large"
            name="title"
            value={comment.title}
            onChange={changeInputHandler}
          />

          <Input.TextArea
            autoSize={{ minRows: 4 }}
            placeholder="Add your comment here"
            name="comment"
            value={comment.comment}
            onChange={changeInputHandler}
          />

          <div className="summit-container">
            <div className="rating">
              <h3>*Rating for us</h3>

              <Rate value={rating} onChange={setRating} />
            </div>
            <div style={{ margin: 'auto 0' }}>
              <button
                className="submit"
                disabled={!rating}
                onClick={postCommentHandler}
              >
                SUBMIT
              </button>
            </div>
          </div>
        </div>
      </Col>
    </Fragment>
  );
}
