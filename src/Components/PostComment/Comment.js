import React, { Fragment } from 'react';
import { Button, Col, Modal, Rate } from 'antd';
import moment from 'moment';
import axios from '../../Util/axios';

export default function Comment(props) {
  const openConfirmModal = () => {
    Modal.confirm({
      title: 'Are you sure?',
      content: 'Are you sure to delete this comment?',
      okText: 'DELETE',
      cancelText: 'CANCEL',
      okButtonProps: {
        danger: true,
        type: 'primary',
      },
      onOk: () => {
        const ratingId = props._id;
        const { productId } = props;

        axios
          .delete(`/rating/${productId}/${ratingId}`)
          .then(() => {
            props.deletePostHandler(ratingId);
          })
          .catch((err) => {
            // console.log(err);
          });
      },
    });
  };

  return (
    <Fragment>
      <Col offset={1} span={4}>
        <div className="name-container">
          <h3>{props.userId.name}</h3>
          <p>{moment(props.createdAt).format('D MMM')}</p>
          {props.isOwner && (
            <Button type="text" onClick={openConfirmModal}>
              Delete
            </Button>
          )}
        </div>
      </Col>
      <Col span={16}>
        <div className="comment-container disable">
          <h3 style={{ fontWeight: 600 }}>{props.title}</h3>
          <Rate value={props.rating} disabled />
          <p>{props.comment}</p>
        </div>
      </Col>
    </Fragment>
  );
}
