import axios from 'axios';

const baseAxios = axios.create({
  baseURL: 'http://localhost:4808/',
});

baseAxios.interceptors.response.use(
  (response) => {
    // console.log(response);
    const isExpiredToken =
      response.data.errors &&
      response.data.errors.find((error) => {
        return error?.extensions?.exception?.code === 401;
      });

    if (isExpiredToken) {
      const refreshToken = sessionStorage.getItem('refreshToken');

      return axios
        .post(`http://localhost:4808/graphql`, {
          query: `
          mutation GetNewToken($refreshToken: String) {
            getNewTokenByRefreshToken(refreshToken: $refreshToken)
          }
        `,
          variables: { refreshToken },
        })
        .then((res) => {
          const {
            data: { getNewTokenByRefreshToken },
            errors,
          } = res.data;

          console.log('new token: ', getNewTokenByRefreshToken);

          if (errors) {
            sessionStorage.removeItem('refreshToken');
            return Promise.reject(errors[0].message);
          }

          setAuthHeader(getNewTokenByRefreshToken);

          const config = response.config;
          config.headers[
            'Authorization'
          ] = `Bearer ${getNewTokenByRefreshToken}`;
          config.baseURL = 'http://localhost:4808/';
          return baseAxios(config);
        })
        .catch((err) => {
          console.log(err);
        });
    }
    return response;
  },
  (error) => {
    const originalRequest = error.config;
    if (error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true;

      const refreshToken = sessionStorage.getItem('refreshToken');
      return axios
        .post(`http://localhost:4808/graphql`, {
          query: `
          mutation GetNewToken($refreshToken: String) {
            getNewTokenByRefreshToken(refreshToken: $refreshToken)
          }
        `,
          variables: { refreshToken },
        })
        .then((res) => {
          const {
            data: { getNewTokenByRefreshToken },
            errors,
          } = res.data;

          console.log('new token: ', getNewTokenByRefreshToken);

          if (errors) {
            sessionStorage.removeItem('refreshToken');
            return Promise.reject(errors[0].message);
          }

          setAuthHeader(getNewTokenByRefreshToken);

          originalRequest.headers[
            'Authorization'
          ] = `Bearer ${getNewTokenByRefreshToken}`;
          originalRequest.baseURL = 'http://localhost:4808/';
          return baseAxios(originalRequest);
        })
        .catch((err) => {
          console.log(err);
        });
    }
    return Promise.reject(error);
  }
);

export const setAuthHeader = (token) => {
  baseAxios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
};

export const clearAuthHeader = () => {
  delete baseAxios.defaults.headers.common['Authorization'];
};

export default baseAxios;
